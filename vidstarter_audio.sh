#!/bin/bash
#
ADRESSE=$(xsel -b)
TITLE=$(youtube-dl -e $ADRESSE)

default_download () {
 notify-send "Download mp3" "$TITLE" --icon=/usr/share/icons/vidstarter-audio.png
 gnome-terminal --title "VidStarter mp3 Download" --command "youtube-dl -x --audio-format mp3 --audio-quality 0 $ADRESSE"
}

error_download () {
 notify-send "Error?" "=(" --icon=/usr/share/icons/vidstarter-audio.png
 gnome-terminal --title "VidStarter mp3 Download" --command "youtube-dl -x --audio-format mp3 --audio-quality 0 $ADRESSE"
}

if [ -z "${TITLE}" ] ; then
 error_download

else
 default_download

fi