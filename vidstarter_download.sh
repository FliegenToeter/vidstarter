#!/bin/bash
#
ADRESSE=$(xsel -b)
TITLE=$(youtube-dl -e $ADRESSE)

error_download () {
 notify-send "Error?" "=(" --icon=/usr/share/icons/vidstarter-video.png
 gnome-terminal --title "VidStarter Download" --command "youtube-dl $ADRESSE"
}

default_download () {
 notify-send "Download" "$TITLE" --icon=/usr/share/icons/vidstarter-video.png
 gnome-terminal --title "VidStarter Download" --command "youtube-dl $ADRESSE"
}

youtube_download () {
 notify-send "Youtube Download" "$TITLE" --icon=/usr/share/icons/vidstarter-video.png
 gnome-terminal --title "VidStarter Download" --command "youtube-dl -f 45/22/247+bestaudio/136+bestaudio/35/44/244+bestaudio/135+bestaudio/43/18/243+bestaudio/242+bestaudio/134+bestaudio/242+bestaudio/133+bestaudio/36/5/278+bestaudio/160+bestaudio/17 $ADRESSE"
}

if [ -z "${TITLE}" ] ; then
 error_download
 
elif printf '%s' "$ADRESSE" | egrep -q "youtube.com/" ; then
 youtube_download
 
elif printf '%s' "$ADRESSE" | egrep -q "youtu.be/" ; then
 youtube_download

else
 default_download

fi