#!/bin/bash
#
ADRESSE=$(xsel -b)
TITLE=$(youtube-dl -e $ADRESSE)

youtube_starter () {
 notify-send "Youtube Stream" "$TITLE" --icon=/usr/share/icons/vidstarter1080.png
 mpv $ADRESSE \
--ytdl-format=46/37/248+bestaudio/137+bestaudio/45/22/247+bestaudio/136+bestaudio/35/44/244+bestaudio/135+bestaudio/43/18/243+bestaudio/242+bestaudio/134+bestaudio/242+bestaudio/133+bestaudio/36/5/278+bestaudio/160+bestaudio/17 \
 --title="$TITLE" \
 --cache=auto \
 --cache-default=45000 \
 --autofit-larger=100%x100% \
 --autofit-smaller=100%x100% \
 --geometry=50%:50% \
 --demuxer-thread=yes \
 --demuxer-readahead-secs=17 \
 --cache-secs=17 \
 --cache-initial=2048
}

twitch_starter () {
 notify-send "Twitch Stream" "$TITLE" --icon=/usr/share/icons/vidstarter1080.png
 mpv $ADRESSE \
 --ytdl-format=Source/High/Medium/Low/Mobile \
 --title="$TITLE" \
 --cache=auto \
 --cache-default=45000 \
 --autofit-larger=100%x100% \
 --autofit-smaller=100%x100% \
 --geometry=50%:50% \
 --demuxer-thread=yes \
 --demuxer-readahead-secs=17 \
 --cache-secs=17 \
 --cache-initial=2048
}

vimeo_starter () {
 notify-send "Vimeo Stream" "$TITLE" --icon=/usr/share/icons/vidstarter1080.png
 mpv $ADRESSE \
 --ytdl-format=h264-hd/h264-sd/h264-mobile \
 --title="$TITLE" \
 --cache=auto \
 --cache-default=30000 \
 --autofit-larger=100%x100% \
 --autofit-smaller=100%x100% \
 --geometry=50%:50% \
 --demuxer-thread=yes \
 --demuxer-readahead-secs=12 \
 --cache-secs=12 \
 --cache-initial=1024
}

yahoo_starter () {
 notify-send "Yahoo Screen Stream" "$TITLE" --icon=/usr/share/icons/vidstarter1080.png
 mpv $ADRESSE \
 --ytdl-format=5/4/3/2/1/0 \
 --title="$TITLE" \
 --cache=auto \
 --cache-default=30000 \
 --autofit-larger=100%x100% \
 --autofit-smaller=100%x100% \
 --geometry=50%:50% \
 --demuxer-thread=yes \
 --demuxer-readahead-secs=12 \
 --cache-secs=12 \
 --cache-initial=1024
}

dailymotion_starter () {
 notify-send "Dailymotion Stream" "$TITLE" --icon=/usr/share/icons/vidstarter1080.png
 mpv $ADRESSE \
 --ytdl-format=hd180/hd/hq/standard/ld \
 --title="$TITLE" \
 --cache=auto \
 --cache-default=45000 \
 --autofit-larger=100%x100% \
 --autofit-smaller=100%x100% \
 --geometry=50%:50% \
 --demuxer-thread=yes \
 --demuxer-readahead-secs=17 \
 --cache-secs=17 \
 --cache-initial=2048
}

default_starter () {
 STREAM=$(youtube-dl -i -g -c $ADRESSE)
 notify-send "Stream" "$TITLE" --icon=/usr/share/icons/vidstarter1080.png
 mpv $STREAM \
 --title="$TITLE" \
 --cache=auto \
 --cache-default=400000 \
 --autofit-larger=100%x100% \
 --autofit-smaller=100%x100% \
 --geometry=50%:50% \
 --demuxer-thread=yes \
 --demuxer-readahead-secs=12 \
 --cache-secs=12 \
 --cache-initial=1024
}

error_starter () {
 STREAM=$(youtube-dl -i -g -c $ADRESSE)
 notify-send "Error?" "=(" --icon=/usr/share/icons/vidstarter1080.png
 mpv $STREAM \
 --title="$TITLE" \
 --cache=auto \
 --cache-default=40000 \
 --autofit-larger=100%x100% \
 --autofit-smaller=100%x100% \
 --geometry=50%:50% \
 --demuxer-thread=yes \
 --demuxer-readahead-secs=12 \
 --cache-secs=12 \
 --cache-initial=1024
}

if [ -z "${TITLE}" ] ; then
 error_starter
 
elif printf '%s' "$ADRESSE" | egrep -q "youtube.com/" ; then
 youtube_starter
 
elif printf '%s' "$ADRESSE" | egrep -q "youtu.be/" ; then
 youtube_starter

elif printf '%s' "$ADRESSE" | egrep -q "twitch.tv/" ; then
 twitch_starter

elif printf '%s' "$ADRESSE" | egrep -q "vimeo.com/" ; then
 vimeo_starter

elif printf '%s' "$ADRESSE" | egrep -q "screen.yahoo.com/" ; then
 yahoo_starter

elif printf '%s' "$ADRESSE" | egrep -q "dailymotion.com/" ; then
 dailymotion_starter

else
 default_starter

fi