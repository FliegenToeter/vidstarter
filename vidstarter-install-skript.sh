#!/bin/bash
#
#Simple installer for VidStarter
# Terminal:
#Functions
ubuntu_all () {
#Figure out the version of Ubuntu that you're running
V=$(/usr/bin/lsb_release -rs)
if [ $V = "14.04" ] || [ $V = "14.04.1" ] || [ $V = "14.04.2" ] || [ $V 
= "14.04.3" ] || [ $V = "14.04.4" ]; then 
  echo "PPAs für Ubuntu 14.04 hinzufügen"
  sudo add-apt-repository ppa:mc3man/mpv-tests && sudo 
add-apt-repository ppa:mc3man/trusty-media
fi
if [ $V = "14.10" ]; then
  echo "PPAs für Ubuntu 14.10 hinzufügen"
  sudo add-apt-repository ppa:mc3man/mpv-tests
fi
  sudo apt-get update
  echo "Benötigte Programme installieren"
  sudo apt-get install -y libav-tools mpv ubuntu-restricted-extras xsel 
ffmpeg libnotify-bin gnome-terminal
  sudo wget https://yt-dl.org/downloads/latest/youtube-dl -O 
/usr/local/bin/youtube-dl
  sudo chmod a+x /usr/local/bin/youtube-dl
}

arch_all () {
  echo "Benötigte Programme installieren"
  sudo pacman -S mpv youtube-dl xsel ffmpeg libnotify gnome-terminal 
}
 
installer_unity () {
# Download skripts and .desktop file
  echo "Download Skripte und Starter"
  sudo wget -q 
https://gitlab.com/FliegenToeter/vidstarter/raw/master/vidstarter_audio.sh 
-O /usr/local/bin/vidstarter_audio.sh
  sudo wget -q 
https://gitlab.com/FliegenToeter/vidstarter/raw/master/vidstarter_download.sh 
-O /usr/local/bin/vidstarter_download.sh
  sudo wget -q 
https://gitlab.com/FliegenToeter/vidstarter/raw/master/vidstarter_stream1080p.sh 
-O /usr/local/bin/vidstarter_stream1080p.sh
  sudo wget -q 
https://gitlab.com/FliegenToeter/vidstarter/raw/master/vidstarter_stream360p.sh 
-O /usr/local/bin/vidstarter_stream360p.sh
  sudo wget -q 
https://gitlab.com/FliegenToeter/vidstarter/raw/master/vidstarter_stream480p.sh 
-O /usr/local/bin/vidstarter_stream480p.sh
  sudo wget -q 
https://gitlab.com/FliegenToeter/vidstarter/raw/master/vidstarter_stream720p.sh 
-O /usr/local/bin/vidstarter_stream720p.sh
  sudo wget -q 
https://gitlab.com/FliegenToeter/vidstarter/raw/master/vidstarter_streambest.sh 
-O /usr/local/bin/vidstarter_streambest.sh
  sudo wget -q 
https://gitlab.com/FliegenToeter/vidstarter/raw/master/vidstarter_streamdefault.sh 
-O /usr/local/bin/vidstarter_streamdefault.sh
  echo "Download .desktop file"
  sudo wget -q 
https://gitlab.com/FliegenToeter/vidstarter/raw/master/vidstarter.desktop 
-O /usr/share/applications/vidstarter.desktop
#
# Download icon
  echo "Download Icons"
  sudo wget -q --no-check-certificate 
https://www.picflash.org/viewer.php?img=yt_streamO7QSL3.png -O 
/usr/share/icons/vidstarter.png
  sudo wget -q --no-check-certificate 
https://www.picflash.org/viewer.php?img=vidstarter-360GG1VS2.png -O 
/usr/share/icons/vidstarter360.png
  sudo wget -q --no-check-certificate 
https://www.picflash.org/viewer.php?img=vidstarter-480YTXYW0.png -O 
/usr/share/icons/vidstarter480.png
  sudo wget -q --no-check-certificate 
https://www.picflash.org/viewer.php?img=vidstarter-7206HFCUF.png -O 
/usr/share/icons/vidstarter720.png
  sudo wget -q --no-check-certificate 
https://www.picflash.org/viewer.php?img=vidstarter-1080V8TVIJ.png -O 
/usr/share/icons/vidstarter1080.png
  sudo wget -q --no-check-certificate 
https://www.picflash.org/viewer.php?img=vidstarter-audio9EI3BT.png -O 
/usr/share/icons/vidstarter-audio.png
  sudo wget -q --no-check-certificate 
https://www.picflash.org/viewer.php?img=vidstarter-bestSQQLG6.png -O 
/usr/share/icons/vidstarter-best.png
  sudo wget -q --no-check-certificate 
https://www.picflash.org/viewer.php?img=vidstarter-video3ARZXM.png -O 
/usr/share/icons/vidstarter-video.png

#
# Make files executable
  echo "Skripte ausführbar machen"
  sudo chmod 755 /usr/share/applications/vidstarter.desktop
  sudo chmod 755 /usr/local/bin/vidstarter_audio.sh
  sudo chmod 755 /usr/local/bin/vidstarter_download.sh
  sudo chmod 755 /usr/local/bin/vidstarter_stream1080p.sh
  sudo chmod 755 /usr/local/bin/vidstarter_stream360p.sh
  sudo chmod 755 /usr/local/bin/vidstarter_stream480p.sh
  sudo chmod 755 /usr/local/bin/vidstarter_stream720p.sh
  sudo chmod 755 /usr/local/bin/vidstarter_streambest.sh
  sudo chmod 755 /usr/local/bin/vidstarter_streamdefault.sh
}
 
installer_all () {
 echo "Download Starter"
  sudo wget -q 
https://gitlab.com/FliegenToeter/vidstarter/raw/master/vidstarter360p.desktop 
-O /usr/share/applications/vidstarter360p.desktop
  sudo wget -q 
https://gitlab.com/FliegenToeter/vidstarter/raw/master/vidstarter480p.desktop 
-O /usr/share/applications/vidstarter480p.desktop
  sudo wget -q 
https://gitlab.com/FliegenToeter/vidstarter/raw/master/vidstarter720p.desktop 
-O /usr/share/applications/vidstarter720p.desktop
  sudo wget -q 
https://gitlab.com/FliegenToeter/vidstarter/raw/master/vidstarter1080p.desktop 
-O /usr/share/applications/vidstarter1080p.desktop
  sudo wget -q 
https://gitlab.com/FliegenToeter/vidstarter/raw/master/vidstarterbest.desktop 
-O /usr/share/applications/vidstarterbest.desktop
  sudo wget -q 
https://gitlab.com/FliegenToeter/vidstarter/raw/master/vidstarteraudio.desktop 
-O /usr/share/applications/vidstarteraudio.desktop
  sudo wget -q 
https://gitlab.com/FliegenToeter/vidstarter/raw/master/vidstarterdownload.desktop 
-O /usr/share/applications/vidstarterdownload.desktop
  echo "Starter ausführbar machen"
  sudo chmod 755 /usr/share/applications/vidstarter360p.desktop
  sudo chmod 755 /usr/share/applications/vidstarter480p.desktop
  sudo chmod 755 /usr/share/applications/vidstarter720p.desktop
  sudo chmod 755 /usr/share/applications/vidstarter1080p.desktop
  sudo chmod 755 /usr/share/applications/vidstarterbest.desktop
  sudo chmod 755 /usr/share/applications/vidstarteraudio.desktop
  sudo chmod 755 /usr/share/applications/vidstarterdownload.desktop
}

#User input
echo "VidStarter Setup"
echo " Was möchten Sie tun?"
echo " Bitte die [Zahl] ohne Klammern eingeben, die für das gewünschte 
Verhalten steht und mit ENTER bestätigen."
echo " [Zahl]  -  Verhalten"
echo " "
echo "Komplette Installation:"
echo "- Ubuntu"
echo " [1] - Unity oder Gnome3 Oberfläche (Quicklists)"
echo " [2] - Andere Oberflächen (XFCE, LXDE, KDE etc.)"
echo "- Arch/Manjaro"
echo " [3] - Gnome3 oder Unity Oberfläche (Quicklists)"
echo " [4] - Andere Oberflächen (XFCE, LXDE, KDE etc.)"
echo " "
echo "- Nur VidStarter Skripte und Starter:\033[0m"
echo " Folgendes muss bereits installiert sein: mpv, youtube-dl, xsel, 
ffmpeg, libnotify, gnome-terminal"
echo " [5] -     Nur 1 Starter mit Quicklists"
echo " [6] -     Alle Starter"
echo " "
echo "Eingabe:"

#Check user input
read nummer
if [ $nummer -eq 1 ] ; then
 echo "Komplette Installation Ubuntu Unity/Gnome3"
 installer_unity 
 ubuntu_all
elif [ $nummer -eq 2 ] ; then
 echo "Komplette Installation Ubuntu alle Starter"
 installer_unity 
 installer_all
 ubuntu_all
elif [ $nummer -eq 3 ] ; then 
 echo "Komplette Installation Arch Gnome3/Unity"
 installer_unity
 arch_all
elif [ $nummer -eq 4 ] ; then
 echo "Komplette Installation Arch alle Starter"
 installer_unity 
 installer_all
 arch_all
elif [ $nummer -eq 5 ] ; then 
 echo "Installation der Skripte und Quicklist-Starter"
 installer_unity
elif [ $nummer -eq 6 ] ; then 
 echo "Installation der Skripte und aller Starter"
 installer_unity 
 installer_all
else echo Fehler - Die angegebene Zahl muss zwischen 1-6 liegen. 
fi ;
