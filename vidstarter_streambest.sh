#!/bin/bash
#
ADRESSE=$(xsel -b)
TITLE=$(youtube-dl -e $ADRESSE)

youtube_starter () {
 notify-send "Youtube Stream" "$TITLE" --icon=/usr/share/icons/vidstarter-best.png
 mpv $ADRESSE \
--ytdl-format=315+bestaudio/308+bestaudio/303+bestaudio/299+bestaudio/302+bestaudio/298+bestaudio/313+bestaudio/266+bestaudio/138+bestaudio/271+bestaudio/264+bestaudio/bestvideo+bestaudio \
--title="$TITLE" \
--cache=auto \
--cache-default=90000 \
--autofit-larger=100%x100% \
--autofit-smaller=100%x100% \
--geometry=50%:50% \
--demuxer-thread=yes \
--demuxer-readahead-secs=17 \
--cache-secs=17 \
--cache-initial=2048
}

default_starter () {
 STREAM=$(youtube-dl -i -g -c $ADRESSE)
 notify-send "Stream" "$TITLE" --icon=/usr/share/icons/vidstarter-best.png
 mpv $STREAM \
 --title="$TITLE" \
 --cache=auto \
 --cache-default=400000 \
 --autofit-larger=100%x100% \
 --autofit-smaller=100%x100% \
 --geometry=50%:50% \
 --demuxer-thread=yes \
 --demuxer-readahead-secs=12 \
 --cache-secs=12 \
 --cache-initial=1024
}

error_starter () {
 STREAM=$(youtube-dl -i -g -c $ADRESSE)
 notify-send "Error?" "=(" --icon=/usr/share/icons/vidstarter-best.png
 mpv $STREAM \
 --title="$TITLE" \
 --cache=auto \
 --cache-default=40000 \
 --autofit-larger=100%x100% \
 --autofit-smaller=100%x100% \
 --geometry=50%:50% \
 --demuxer-thread=yes \
 --demuxer-readahead-secs=12 \
 --cache-secs=12 \
 --cache-initial=1024
}

if [ -z "${TITLE}" ] ; then
 error_starter
 
elif printf '%s' "$ADRESSE" | egrep -q "youtube.com/" ; then
 youtube_starter
 
elif printf '%s' "$ADRESSE" | egrep -q "youtu.be/" ; then
 youtube_starter

else
 default_starter

fi