#!/bin/bash
#
ADRESSE=$(xsel -b)
TITLE=$(youtube-dl -e $ADRESSE)

default_starter () {
 STREAM=$(youtube-dl -i -g -c $ADRESSE)
 notify-send "Stream" "$TITLE" --icon=/usr/share/icons/vidstarter.png
 mpv $STREAM \
 --title="$TITLE" \
 --cache=auto \
 --cache-default=400000 \
 --autofit-larger=100%x100% \
 --autofit-smaller=100%x100% \
 --geometry=50%:50% \
 --demuxer-thread=yes \
 --demuxer-readahead-secs=12 \
 --cache-secs=12 \
 --cache-initial=1048
}

youtube_starter () {
 notify-send "Youtube Stream" "$TITLE" --icon=/usr/share/icons/vidstarter.png
 mpv $ADRESSE \
--ytdl-format=22/45/44/35/43/18/34/5/36/17 \
 --title="$TITLE" \
 --cache=auto \
 --cache-default=35000 \
 --autofit-larger=100%x100% \
 --autofit-smaller=100%x100% \
 --geometry=50%:50% \
 --demuxer-thread=yes \
 --demuxer-readahead-secs=12 \
 --cache-secs=12 \
 --cache-initial=1024
}

error_starter () {
 STREAM=$(youtube-dl -i -g -c $ADRESSE)
 notify-send "Error?" "=(" --icon=/usr/share/icons/vidstarter.png
 mpv $STREAM \
 --title="$TITLE" \
 --cache=auto \
 --cache-default=40000 \
 --autofit-larger=100%x100% \
 --autofit-smaller=100%x100% \
 --geometry=50%:50% \
 --demuxer-thread=yes \
 --demuxer-readahead-secs=12 \
 --cache-secs=12 \
 --cache-initial=1024
}

if [ -z "${TITLE}" ] ; then
 error_starter
 
elif printf '%s' "$ADRESSE" | egrep -q "youtube.com/" ; then
 youtube_starter
 
elif printf '%s' "$ADRESSE" | egrep -q "youtu.be/" ; then
 youtube_starter

else
 default_starter

fi